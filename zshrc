# Path to your oh-my-zsh installation.
export ZSH="/home/narayan/.oh-my-zsh"

#-----------------------
# Themes
#-----------------------

#ZSH_THEME="fwalch"
ZSH_THEME="geometry/geometry"

#https://github.com/geometry-zsh/geometry
PROMPT_GEOMETRY_COLORIZE_SYMBOL=true

#-----------------------
# Plugins
#-----------------------
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/

plugins=(
  fast-syntax-highlighting
  npm
  sudo
  fasd
  #solarized-man
  #tmux
  #kubectl
)

source $ZSH/oh-my-zsh.sh
#source ~/.oneconfig

#------------------------
# User configuration
#-----------------------

# Start ssh-agent to store ssh-key
if [ ! -S ~/.ssh/ssh_auth_sock ]; then
  eval `ssh-agent`
  ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
fi

export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock

function lazygit() {
  if [ -d .git ]; then
    git status
    git add .
    git commit -a -m "$1"
    git push
  fi;
}

function genpass() {
#openssl rand -base64 29 | tr -d "=+/" | cut -c1-25
LENGTH=25
if [ ! -z "$1" ] && [ $1 -gt 1 ]; then
    LENGTH=$1
  fi
  NUMBYTES=`echo $LENGTH | awk '{print int($1*1.16)+1}'`

  openssl rand -base64 $NUMBYTES | tr -d "=+/" | cut -c1-$LENGTH

}


export VISUAL=vim
export EDITOR="$VISUAL"

alias godot="/home/narayan/Lab/git/dot-files"
alias cpwd="pwd | xclip -selection clipboard"
alias notes="vim ~/Documents/Notes/"
alias ehosts="sudoedit /etc/hosts"
alias essh="vim ~/.ssh/config"
alias ezsh="vim ~/.zshrc"
alias evim="vim ~/.vimrc"
alias eawesome="vim ~/.config/awesome/rc.lua"
alias eranger="vim ~/.config/ranger/rc.conf"
alias etermite="vim ~/.config/termite/config"
alias iris-day="bash ~/.config/iris-micro/day_mode.sh"
alias iris-day2="bash ~/.config/iris-micro/day2_mode.sh"
alias iris-night="bash ~/.config/iris-micro/night_mode.sh"
alias iris-night2="bash ~/.config/iris-micro/night2_mode.sh"
alias iris-reset="bash ~/.config/iris-micro/reset.sh"
alias kb-us="setxkbmap -layout 'us' -model 'geniuskkb2050hs' -v 9 -symbols pc+us+inet -option  && setxkbmap -option caps:super"
alias super='setxkbmap -option caps:super'
alias kb-col="bash /home/narayan/.config/BigBagKbdTrixXKB/setxkb.sh 4c us us && setxkbmap -option caps:super"
alias dvo='setxkbmap -layout "us(dvorak)" -v 9 && setxkbmap -option caps:super'

#alias rg="rg --smart-case"

# Work
alias oneconfig="/home/narayan/go/src/bitbucket.org/oneconfig/"


alias a='fasd -a'        # any
alias s='fasd -si'       # show / search / select
alias d='fasd -d'        # directory
alias f='fasd -f'        # file
alias sd='fasd -sid'     # interactive directory selection
alias sf='fasd -sif'     # interactive file selection
alias z='fasd_cd -d'     # cd, same functionality as j in autojump
alias zz='fasd_cd -d -i' # cd with interactive selection

## Options section
setopt correct                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt nocheckjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt autocd                                                   # if only directory path is entered, cd there.

# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='vim'
 fi


# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"
export GOPATH=$HOME/go
export PATH=$HOME/bin:/usr/local/bin:/home/narayan/.local/bin:$GOPATH/bin:$PATH

# Color man pages
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r

# Enable keyboard Shortcuts
#bindkey '^[[3~' delete-char
#bindkey "\033[1~" beginning-of-line
#bindkey "\033[4~" end-of-line
#bindkey '^D' delete-char

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Added to autostart.sh
#eval "$(fasd --init posix-alias zsh-hook)"
# remap all keys to default layout
# setxkbmap -layout us -option
# remap caps to super
#setxkbmap -option caps:super

export TERM=xterm-256color
export TERMINAL="termite"
TZ='Africa/Johannesburg'; export TZ

neofetch

export N_PREFIX="$HOME/n"; [[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin"  # Added by n-install (see http://git.io/n-install-repo).

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
