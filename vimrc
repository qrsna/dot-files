"  Vundle [Plugin Manager]
"  ---------------------------------------------------------------------------

filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Plugin Manager
Plugin 'VundleVim/Vundle.vim' " Plugin manager

" General
Plugin 'scrooloose/nerdtree' " IDE Like File browser
Plugin 'kien/ctrlp.vim' " Filesearch
Plugin 'nathanaelkane/vim-indent-guides'  " Visually display indenting
Plugin 'jremmen/vim-ripgrep' " Search for strings in files
Plugin 'tpope/vim-sensible' " Extra settings for vim
Plugin 'simeji/winresizer' " Manage (move,resize) vim panes
Plugin 'vimwiki/vimwiki' " Vim wiki
"Plugin 'maksimr/vim-jsbeautify' " Format JS HTML CSS #Must install

" Development
Plugin 'moll/vim-node' " Node Shortcuts
Plugin 'Valloric/YouCompleteMe' " AutoComplete
Plugin 'ternjs/tern_for_vim' " Tern-based JavaScript editing support
Plugin 'w0rp/ale' " Linting for everything
Plugin 'Raimondi/delimitMate' " Closes brackets
Plugin 'scrooloose/nerdcommenter' " Comment blocks for any language
Plugin 'tpope/vim-fugitive' " Git tools
Plugin 'tpope/vim-surround' "  Surround text with brackets easily
Plugin 'tpope/vim-repeat' " Repeat plugin map with '.'
Plugin 'vim-scripts/dbext.vim' " SQL Autocomplete
Plugin 'ntpeters/vim-better-whitespace' " Remove trailing spaces
Plugin 'alvan/vim-closetag' " Closes html tags
Plugin 'ap/vim-css-color' " CSS color name highlighter
Plugin 'skywind3000/asyncrun.vim' " Run shell commands in background and display in window
Plugin 'fatih/vim-go' " Go support

"Syntax
Plugin 'tpope/vim-markdown' " Markdown syntax
Plugin 'jtratner/vim-flavored-markdown' " Extends tpope/vim-markdown
"Plugin 'prettier/vim-prettier' " Linting
Plugin 'leafgarland/typescript-vim' " Typescript syntax
Plugin 'pangloss/vim-javascript' " indentation and syntax
Plugin 'mxw/vim-jsx' " JSX (react) syntax
Plugin 'othree/javascript-libraries-syntax.vim' " Syntax support for all other js languages
Plugin 'vim-ruby/vim-ruby' " Ruby syntax and stuff
Plugin 'elzr/vim-json' " Json Highliter
Plugin 'ekalinin/Dockerfile.vim' " Docker syntax
Plugin 'martinda/Jenkinsfile-vim-syntax' " Jenkinsfile syntax
Plugin 'chr4/nginx.vim' " Nginx syntax
Plugin 'rodjek/vim-puppet' " Puppet syntax
Plugin 'saltstack/salt-vim' "salt-stack syntax
Plugin 'tbastos/vim-lua' " lua syntax
Plugin 'pearofducks/ansible-vim' " Ansible syntax
Plugin 'briancollins/vim-jst' "Handlebars syntax


"Themes
"Plugin 'vim-airline/vim-airline' " Lightweight powerline (Kiff status bar)
"Plugin 'vim-airline/vim-airline-themes' " Themes for status bar
Plugin 'itchyny/lightline.vim' " Lightweight status bar
Plugin 'ap/vim-buftabline'    " Minimal buffer status bar
Plugin 'skielbasa/vim-material-monokai'
Plugin 'liuchengxu/space-vim-dark'

"Plugin 'ryanoasis/vim-devicons' " Icons for nerdtree

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

"  ---------------------------------------------------------------------------
"  Tab settings
"  ---------------------------------------------------------------------------

" show existing tab with 4 spaces width
set tabstop=2
" when indenting with '>', use spaces width
set shiftwidth=2

" set file type for extensions
autocmd BufNewFile,BufRead *.ts set syntax=typescript
autocmd BufNewFile,BufRead *.tsx set syntax=typescript

" On pressing tab, insert spaces
autocmd Filetype html setlocal ts=2 sts=2 sw=2
autocmd Filetype ruby setlocal ts=2 sts=2 sw=2
autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
autocmd Filetype typescript setlocal ts=2 sts=2 sw=2
autocmd Filetype puppet setlocal ts=4 sts=4 sw=4


"  ---------------------------------------------------------------------------
"   Settings
"  ---------------------------------------------------------------------------
set relativenumber
set autowrite " Saves file when build command is run

" Go {
  let g:go_metalinter_autosave = 1
  let g:go_metalinter_autosave_enabled = ['vet', 'golint']

  let g:go_highlight_types = 1
  let g:go_highlight_fields = 1
  let g:go_highlight_functions = 1
  let g:go_highlight_functions = 1
  "Default
  " let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck']
  "golangci-lint

  " format with goimports instead of gofmt
  let g:go_fmt_command = "goimports"
"}

set scrolloff=5 " Display 5 lines above/below the cursor when scrolling with a mouse.
set ttyfast " Speed up scrolling in Vim
set linebreak " Break lines at word (requires Wrap lines)
set showbreak=+++   " Wrap-broken line prefix

" Removes insert mode title
set noshowmode

" Suffixes that get lower priority when doing tab completion for filenames.
" These are files we are not likely to want to edit or read.
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc,.png,.jpg

" Disable omnicomplete from autocompleting error
set completeopt=longest,menuone

set encoding=UTF-8
"set guifont=DroidSansMono\ Nerd\ Font\ 11
set guifont=Consolas\ 12
let g:airline_powerline_fonts = 1

"set ignorecase          " Case insensitive search with '/'
set expandtab           " tabs are spaces
"set number              " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line
filetype indent on      " load filetype-specific indent files
set wildmenu            " visual autocomplete for command menu
set showmatch           " highlight matching [{()}]
set backspace=indent,eol,start  " enables baackspace in insert mode
set clipboard=unnamed  " Use both system clipboard and vim clipboard (Requires gvim installed)
"set clipboard=unnamedplus  " enables global clipboard (Requires gvim installed)
set autoindent                        " indent when creating newline
set mouse=a " enable tmux mouse scrolling in vim
set ttymouse=xterm2 " Enable mouse to resize vim panes with a tmux session

" Try not to change the indent structure on "<<" and ">>" commands. I.e. keep
" block comments aligned with space if there is a space there.
set nopreserveindent

" Smart detect when in braces and parens. Has annoying side effect that it
" won't indent lines beginning with '#'. Relying on syntax indentexpr instead.
" 'smartindent' in general is a piece of garbage, never turn it on.
set nosmartindent

"" turn off auto-insert of comments
augroup auto_comment
      au!
          au FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
        augroup END

augroup markdown
    au!
    au BufNewFile,BufRead *.md,*.markdown setlocal filetype=ghmarkdown
augroup END

" Plugin 'ntpeters/vim-better-whitespace'
let g:strip_whitespace_on_save=1
let g:better_whitespace_enabled=1
let g:strip_whitespace_confirm=0
let g:better_whitespace_filetypes_blacklist=['yaml', 'yml', 'md', 'markdown', 'diff', 'gitcommit', 'unite', 'qf', 'help']
let g:better_whitespace_ctermcolor='Green'

" Fix bug that added an extra '>' when closing html tags
let g:closetag_filenames = "*.html,*.js,*.jsx, *.ts,*.tsx"
au FileType html,js,jsx,ts,tsx let b:delimitMate_matchpairs = "(:),[:],{:}"

let g:ctrlp_working_path_mode = 'rw' " Current working path
" Ignore dirs for faster search
let g:ctrlp_custom_ignore = '\v[\/](node_modules|build|.git|dist)|(\.(swp|ico|git|svn|))$'

set wildignore+=.DS_Store
set wildignore+=*.jpg,*.jpeg,*.gif,*.png,*.gif,*.psd,*.o,*.obj
set wildignore+=*.min.js,*.pyc
set wildignore+=*/bower_components/*,*/node_modules/*,*/build/*,*/.git/*,*/dist/*

" YouCompleteMe {
  let g:ycm_min_num_of_chars_for_completion = 4
  let g:ycm_min_num_identifier_candidate_chars = 4
  let g:ycm_enable_diagnostic_highlighting = 0
  let g:ycm_allow_changing_updatetime = 0

  " Disable preview window
  let g:ycm_autoclose_preview_window_after_insertion = 1
  let g:ycm_autoclose_preview_window_after_completion = 1

  " Close scratch/preview window automatically
  let g:ycm_autoclose_preview_window_after_insertion = 1
  let g:ycm_autoclose_preview_window_after_completion = 1
"}


" Ale Plugin {

  " Change warn highlight
  "highlight ALEWarning ctermbg=DarkMagenta
  "highlight ALEError ctermbg=DarkMagenta
  "highlight ALEError ctermbg=none cterm=underline
  "highlight ALEWarning ctermbg=none cterm=underline
  "highlight ALEError guibg=green ctermbg=green cterm=undercurl
  "highlight ALEWarning guibg=green ctermbg=green cterm=undercurl

  " Set this. Airline will handle the rest.
  let g:airline#extensions#ale#enabled = 1
  "highlight clear ALEErrorSign
  "highlight clear ALEWarningSign

  " Jump to ale errors
  nmap <silent> <M-k> <Plug>(ale_previous_wrap)
  nmap <silent> <M-j> <Plug>(ale_next_wrap)

  let g:ale_completion_enabled = 0 " Autocompletion
  let g:ale_echo_cursor = 1
  let g:ale_echo_msg_error_str = 'Error'
  let g:ale_echo_msg_format = '%s'
  let g:ale_echo_msg_warning_str = 'Warning'
  let g:ale_enabled = 1
  let g:ale_keep_list_window_open = 0
  let g:ale_lint_delay = 200
  let g:ale_lint_on_enter = 1
  let g:ale_lint_on_save = 1
  let g:ale_lint_on_text_changed = 'always'
  let g:ale_fix_on_save = 0
  let g:ale_open_list = 0
  let g:ale_set_highlights = 0
  let g:ale_set_loclist = 1
  let g:ale_set_quickfix = 0
  let g:ale_set_signs = 1
  let g:ale_sign_column_always = 0
  let g:ale_sign_error = '✖'
  let g:ale_sign_offset = 1000000
  let g:ale_sign_warning = '⇢'
  let g:ale_statusline_format = ['%d error(s)', '%d warning(s)', 'OK']
  let g:ale_warn_about_trailing_whitespace = 0
  "let g:ale_linter_aliases = {}
  let g:ale_linters = {'html': [], 'javascript': ['eslint'], 'typescript': ['eslint']}

  let g:ale_fixers = {
  \   'javascript': ['eslint'],
  \   'typesript': ['eslint'],
  \}

"}

" Airline Plugin {
  let g:airline_theme='kolor'
  let g:airline_powerline_fonts = 1
  " enable/disable syntastic integration >
  let g:airline#extensions#syntastic#enabled = 1

  " enable/disable fugitive/lawrencium integration
  let g:airline#extensions#branch#enabled = 1
  " to only show the tail, e.g. a branch 'feature/foo' becomes 'foo', use
  let g:airline#extensions#branch#format = 1

  " enable/disable YCM integration
  let g:airline#extensions#ycm#enabled = 1

  " Enable the list of buffers
  let g:airline#extensions#tabline#enabled = 1

  " Show just the filename
  "let g:airline#extensions#tabline#fnamemod = ':t'
"}

" Set vimwiki to use markdown
" https://youtu.be/ONh95PNBW-Q
" https://github.com/gotbletu/shownotes/blob/master/vimwiki.md
let g:vimwiki_list = [{'path': '~/Lab/git/vimwiki/'}]
"                     \ 'syntax': 'markdown', 'ext': '.md'}]



"  ---------------------------------------------------------------------------
"     Shortcuts
"  ---------------------------------------------------------------------------

" Copy file full path
nnoremap <Leader>c :let @+=expand('%:p')<CR>

" Set Leader Key
let mapleader=" "

" Buffer switching using TAB and 'wildmenu'
set wildcharm=<Tab>
set wildmenu
set wildmode=full
nnoremap <leader><Tab> :buffer<Space><Tab>

" SplitLayout navigations
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

" Copy to systemclipboard ctrl + c
vmap <C-c> "+y
" Cut to systemclipboard ctrl + x
vmap <C-x> "+c
" Paste from systemclipboard ctrl + p
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+

"nnoremap <SPACE> za " Enable folding with the spacebar

" enable/disable highlighting quickly
noremap <F3> :set hlsearch! hlsearch?<CR>

" Toggle Indent Guide
nmap <leader>i :IndentGuidesToggle<CR>

" Buffer {
  " This allows buffers to be hidden if you've modified a buffer.
  " This is almost a must if you wish to use buffers in this way.
  set hidden

  " To open a new empty buffer
  " This replaces :tabnew which I used to bind to this mapping
  nmap <leader>t :enew<cr>

  " Move to the next buffer
  nmap <leader>j :bnext<CR>

  " Move to the previous buffer
  nmap <leader>k :bprevious<CR>

  " Close the current buffer and move to the previous one
  " This replicates the idea of closing a tab
  nmap <leader>q :bp <BAR> bd #<CR>

  " Show all open buffers and their status
  nmap <leader>l :ls<CR>

  " Easy bindings for its various modes
  nmap <leader>bb :CtrlPBuffer<cr>
  nmap <leader>bm :CtrlPMixed<cr>
  nmap <leader>bs :CtrlPMRU<cr>
" }

" Quick run via <F5> {
  nnoremap <F5> :call <SID>compile_and_run()<CR>

  function! s:compile_and_run()
      exec 'w'
      if &filetype == 'c'
          exec "AsyncRun! gcc % -o %<; time ./%<"
      elseif &filetype == 'cpp'
         exec "AsyncRun! g++ -std=c++11 % -o %<; time ./%<"
      elseif &filetype == 'sh'
         exec "AsyncRun! time bash %"
      elseif &filetype == 'python'
         exec "AsyncRun! time python %"
      elseif &filetype == 'javascript'
         exec "AsyncRun! time node %"
      endif
  endfunction

  " asyncrun now has an option for opening quickfix automatically
  let g:asyncrun_open = 15

  " F6 to toggle quickfix window
  nnoremap <F6> :call asyncrun#quickfix_toggle(6)<cr>
"}

" Go {
autocmd FileType go nmap <leader>b  <Plug>(go-build)
autocmd FileType go nmap <leader>r  <Plug>(go-run)
"}


  "jsbeatify
  "map <c-g> :call JsBeautify()<cr>
  "autocmd FileType javascript vnoremap <buffer>  <c-g> :call RangeJsBeautify()<cr>
  "autocmd FileType json vnoremap <buffer> <c-g> :call RangeJsonBeautify()<cr>
  "autocmd FileType jsx vnoremap <buffer> <c-g> :call RangeJsxBeautify()<cr>
  "autocmd FileType html vnoremap <buffer> <c-g> :call RangeHtmlBeautify()<cr>
  "autocmd FileType css vnoremap <buffer> <c-g> :call RangeCSSBeautify()<cr>

" Set nopaste and paste
" set pastetoggle=<F2>
"set paste  " Disable automatic comment insertion

"  ---------------------------------------------------------------------------
"    NerdTree
"  ---------------------------------------------------------------------------

" Open NerdTree by default
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists(“s:std_in”) | NERDTree | endif

" Find
map <Leader>v :NERDTreeFind<CR>
" Toggle
map <Leader>f :NERDTreeToggle<Enter>
"nnoremap <F4> :NERDTreeToggle

" Close Automatically
let NERDTreeQuitOnOpen = 1

" Automatically delete the buffer of the file you just deleted with NerdTree:
let NERDTreeAutoDeleteBuffer = 1

" Make Vim prettier "Remove 'press ? for help'
let NERDTreeMinimalUI = 0
let NERDTreeDirArrows = 1

let NERDTreeShowHidden=1  " show hidden files in nerdtree
let NERDTreeShowBookmarks=1 " Display NERDTree Bookmarks

"NERDTree filter out file extentions
let NERDTreeIgnore = ['\.swo$', '\.swp$']


"let g:NERDTreeChDirMode = 2 " Current working path

"  ---------------------------------------------------------------------------
"     Move vim files to central location
"  ---------------------------------------------------------------------------

" swap files (.swp) in a common location
" // means use the file's full path
set dir=~/.vim/_swap//

" backup files (~) in a common location if possible
set backup
set backupdir=~/.vim/_backup/,.

" turn on undo files, put them in a common location
set undofile
set undodir=~/.vim/_undo/


"  ---------------------------------------------------------------------------
"     Colour Schemes
"  ---------------------------------------------------------------------------

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

"colorscheme material-monokai
colorscheme space-vim-dark

syntax enable
set background=dark
set cursorline          " highlight current line

" Colours for Indent Guide
hi IndentGuidesOdd  ctermbg=black
hi IndentGuidesEven ctermbg=darkgrey

